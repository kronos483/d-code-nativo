#include <walnut/Application.h>
#include <walnut/EntryPoint.h>
#include <walnut/Image.h>
#include <walnut/Utils.h>
#include <walnut/icons/IconsForkAwesome.h>
#include <walnut/icons/emoji.h>

#include <imgui_stdlib.h>

#include <iostream>
#include <array>
#include <chrono>
#include <thread>

#include "backends/imgui_impl_vulkan.h"

static VkDescriptorSet m_DescriptorSet;

class ExampleLayer : public Walnut::Layer
{
public:
	Walnut::Image g_Image = Walnut::Image("D:/Ghostscript_Tiger.svg");
	virtual void OnUIRender() override
	{
		static std::string buff;
		buff.reserve(1024);

		ImGui::Begin("Hello");
			static int selected = -1;
			ImGui::BeginButtonGroup(ICON_FK_ALIGN_LEFT, &selected, 0, {30, 25});
				ImGui::ButtonGroup(ICON_FK_ALIGN_CENTER, &selected, 1, 30);
				ImGui::ButtonGroup(ICON_FK_ALIGN_RIGHT, &selected, 2, 30);
			ImGui::EndButtonGroup(ICON_FK_ALIGN_JUSTIFY, &selected, 3, 30);


			const char* items[] = { "AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIIIIII", "JJJJ", "KKKKKKK" };
			static int item_current = 0;
			ImGui::Combo("combo", &item_current, items, IM_ARRAYSIZE(items));
			static bool boolean = false;
			ImGui::Checkbox("Checkbox", &boolean);
			if (ImGui::ActiveButton("Active Button")) {
				static auto i = 0;
				std::cout << "Active button pressed " << ++i << std::endl;
			}
		ImGui::End();

		static float f = 0.0f;

		ImGui::Begin("Progress Indicators");
			ImGui::Spinner("##spinner", 15, 6);
			ImGui::BufferingBar("##buffer_bar", f, ImVec2(400, 6));
			static bool val = false;
			
			if (ImGui::BeginTable("toggle table", 2)) {
				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text("Test toggle:");
				ImGui::TableNextColumn();
				ImGui::ToggleSlider("##Toggle_button", &val);
				ImGui::EndTable();
			}

			if (val)
				ImGui::ToggleButton(ICON_FK_VOLUME_UP, &val);
			else
				ImGui::ToggleButton(ICON_FK_VOLUME_MUTE, &val);

			ImGui::Text(ICON_FK_APPLE " fps: %f", ImGui::GetIO().Framerate);
			ImGui::SliderFloat("😈 test", &f, -1.0f, 2.0f);
			ImGui::Text("🚗🛺🛥🏀⚽🏈🥅🍠🍉🍋🐱🦂🌥⛅😀😈");
			ImGui::InputText("🧮 España", &buff);
			int n = 0;
			{
				ImGui::Text("Emojis:");
				ImGuiStyle& style = ImGui::GetStyle();
				float window_visible_x2 = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;
				constexpr auto emoji_list = EM_LIST_CHAR;
				const char* icon = emoji_list;
				auto length = strlen(icon);
				ImVec2 button_sz(24, 24);
				while (length > 0) {
					ImGui::PushID(n++);
					button_sz = ImGui::CalcTextSize(icon, icon + length);
					button_sz.x += 6.0f;
					button_sz.y += 6.0f;
					if (ImGui::Button(icon, button_sz)) {
						buff += icon;
					}
					icon = icon + length + 1;
					length = strlen(icon);
					ImGui::FlowLayout(button_sz);
					ImGui::PopID();
				}
				ImGui::Text("(%i emojis)", n);
			}
			ImGui::NewLine();
			{
				ImGui::Text("Icons:");
				ImGuiStyle& style = ImGui::GetStyle();
				const char* icon = __ICON_LIST;
				auto length = strlen(icon);
				int start = n;
				ImVec2 button_sz(24, 24);
				while (length > 0) {
					ImGui::PushID(n++);
					if (ImGui::Button(icon, button_sz)) {
						buff += icon;
					}
					icon = icon + length + 1;
					length = strlen(icon);
					ImGui::FlowLayout(button_sz);
					ImGui::PopID();
				}
				ImGui::Text("(%i icons)", (n-start));
			}
			ImGui::End();

		ImGui::ShowDemoWindow();
	}
};

Walnut::Application* Walnut::CreateApplication(int argc, char** argv)
{
	Walnut::ApplicationSpecification spec;
	spec.Name = "Walnut Example";

	Walnut::Application* app = new Walnut::Application(spec);

	app->PushLayer<ExampleLayer>();
	//app->SetMenubarCallback([app]()
	//{
	//	if (ImGui::BeginMenu("File"))
	//	{
	//		if (ImGui::MenuItem("Exit"))
	//		{
	//			app->Close();
	//		}
	//		ImGui::EndMenu();
	//	}
	//});
	return app;
}